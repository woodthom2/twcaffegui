# TW Caffe GUI

TW Caffe GUI is a Java based GUI for viewing (and in a later version, editing) Caffe prototxt models.

# Prerequisites to edit and compile

- Maven
    - Maven will download Java protobuf libraries
    - optional: Maven will download ProGuard, which reduces the size of the JAR.
- Protoc (sudo apt-get install protobuf-compiler)
- Caffe (you don't need to have built it, but you do need the Caffe models definition file caffe.proto to be on your machine)
- Recommended IDE: NetBeans

# Prerequisites to run executable JAR
- only Java
- Download the latest executable from the Downloads page

# Before you start

- Check you have environment variable $CAFFE_ROOT pointing to your Caffe directory.
- Run generate-caffe-java-class-from-protobuf-definition.sh. This will create a Java file Caffe.java, which the project relies on.

## License and Citation

TW Caffe GUI is released under the [GNU General Public License v3.0](http://www.gnu.org/licenses/gpl-3.0.en.html).