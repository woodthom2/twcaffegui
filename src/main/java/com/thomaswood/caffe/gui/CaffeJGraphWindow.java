package com.thomaswood.caffe.gui;

import caffe.Caffe;
import com.google.protobuf.TextFormat;
import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.JGraphLayout;
import com.jgraph.layout.hierarchical.JGraphHierarchicalLayout;
import java.awt.*;
import java.awt.geom.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jgraph.*;
import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.graph.*;

import org.jgrapht.*;
import org.jgrapht.ext.*;
import org.jgrapht.graph.*;

// resolve ambiguity
import org.jgrapht.graph.DefaultEdge;

/**
 * A demo applet that shows how to use JGraph to visualize JGraphT graphs.
 *
 * @author Barak Naveh
 * @since Aug 3, 2003
 */
public class CaffeJGraphWindow
        extends JApplet {

    private static final long serialVersionUID = 3256444702936019250L;
    private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    private static final Dimension DEFAULT_SIZE = new Dimension(530, 9320);
    private static JFrame frame;

    JGraph jgraph;
    Caffe.NetParameter.Builder modelBuilder;
    ListenableGraph<CaffeNode, DefaultEdge> g;
    JScrollPane scrollPane;

    //
    private JGraphModelAdapter<CaffeNode, DefaultEdge> jgAdapter;

    /**
     * An alternative starting point for this demo, to also allow running this
     * applet as an application.
     *
     * @param args ignored.
     */
    public static void main(String[] args) {
        CaffeJGraphWindow applet = new CaffeJGraphWindow();
        applet.init();

        frame = new JFrame();
        frame.getContentPane().add(applet);
        frame.setTitle("TW Caffe Model GUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setJMenuBar(new CaffeGuiMenuBar(applet));

        // END MENU BARS
        frame.pack();
        frame.setVisible(true);

    }
    private String currentPath;

    /**
     * {@inheritDoc}
     */
    public void init() {
        setPreferredSize(new Dimension(530, 400));
        resize(DEFAULT_SIZE);
    }

    private void saveModel(String prototxtFile) throws IOException {

        CaffeModelFileLoader.saveModel(prototxtFile, modelBuilder);
    }

    private void loadAndDisplayModel(String prototxtFile) throws IOException {

        try {

            modelBuilder = CaffeModelFileLoader.loadModel(prototxtFile);

            reinitialiseGraphView();

            addModelToGraph(modelBuilder, g);

            layoutGraph(jgraph);

            // force redraw
            frame.setVisible(false);
            frame.setVisible(true);

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    private void layoutGraph(JGraph jgraph) {
        /// layout
        JGraphLayout layout = new JGraphHierarchicalLayout(); // or whatever layouting algorithm
        //layout = new JGraphFastOrganicLayout();
        JGraphFacade facade = new JGraphFacade(jgraph);
        layout.run(facade);
        Map nested = facade.createNestedMap(false, false);
        jgraph.getGraphLayoutCache().edit(nested);
    }

    private void addModelToGraph(Caffe.NetParameter.Builder builder, ListenableGraph<CaffeNode, DefaultEdge> g) {

        Map<String, BlobNode> blobNodes = new HashMap<String, BlobNode>();

        
        for (Caffe.LayerParameter layer : builder.getLayerList()) {

            CaffeNode layerNode = new LayerNode(layer);
            g.addVertex(layerNode);

            // layers are shown as blue nodes
            DefaultGraphCell cell = jgAdapter.getVertexCell(layerNode);
            AttributeMap attr = cell.getAttributes();
            GraphConstants.setBackground(attr, new Color(0, 0, 127));

            for (String blobName : layer.getBottomList()) {
                BlobNode blobNode = blobNodes.get(blobName);
                if (blobNode == null) {
                    blobNode = new BlobNode(blobName);
                    blobNodes.put(blobName, blobNode);
                    g.addVertex(blobNode);
                }
                g.addEdge(blobNode, layerNode);
            }
            for (String blobName : layer.getTopList()) {
                BlobNode blobNode = new BlobNode(blobName);
                blobNodes.put(blobName, blobNode);
                g.addVertex(blobNode);
                g.addEdge(layerNode, blobNode);
            }
            
        }
    }

    private void adjustDisplaySettings(JGraph jg) {
        jg.setPreferredSize(DEFAULT_SIZE);

        Color c = DEFAULT_BG_COLOR;
        String colorStr = null;

        try {
            colorStr = getParameter("bgcolor");
        } catch (Exception e) {
        }

        if (colorStr != null) {
            c = Color.decode(colorStr);
        }

        jg.setBackground(c);
    }

    void showOpenDialog() {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Caffe model Prototxt", "prototxt");

        fc.addChoosableFileFilter(filter);

        if (currentPath != null) {
            fc.setCurrentDirectory(new File(currentPath));
        }
        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                loadAndDisplayModel(file.getAbsolutePath());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this,
                        "Could not load " + file.getName() + ".\n" + e);
            }
            currentPath = fc.getSelectedFile().getParent();

        } else {
        }

    }

    void showSaveDialog() {
        if (modelBuilder == null) {
            JOptionPane.showMessageDialog(this,
                    "Cannot save an empty model");
            return;
        }

        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("Caffe model Prototxt", "prototxt");

        fc.addChoosableFileFilter(filter);

        if (currentPath != null) {
            fc.setCurrentDirectory(new File(currentPath));
        }
        int returnVal = fc.showSaveDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                saveModel(file.getAbsolutePath());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this,
                        "Could not save " + file.getName() + ".\n" + e);
            }
            currentPath = fc.getSelectedFile().getParent();

        } else {
        }

    }

    private void reinitialiseGraphView() {
        g = new ListenableDirectedMultigraph<CaffeNode, DefaultEdge>(
                DefaultEdge.class);
        jgAdapter = new JGraphModelAdapter<CaffeNode, DefaultEdge>(g);
        jgraph = new JGraph(jgAdapter);
        adjustDisplaySettings(jgraph);

        if (scrollPane != null) {
            remove(scrollPane);
        }
        scrollPane = new JScrollPane(jgraph);
        add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * a listenable directed multigraph that allows loops and parallel edges.
     */
    private static class ListenableDirectedMultigraph<V, E>
            extends DefaultListenableGraph<V, E>
            implements DirectedGraph<V, E> {

        private static final long serialVersionUID = 1L;

        ListenableDirectedMultigraph(Class<E> edgeClass) {
            super(new DirectedMultigraph<V, E>(edgeClass));
        }
    }

    interface CaffeNode {
    }

    class LayerNode implements CaffeNode {

        private final Caffe.LayerParameter layer;

        private LayerNode(Caffe.LayerParameter layer) {
            this.layer = layer;
        }

        @Override
        public String toString() {
            return "LAYER " + layer.getName();
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(this.layer);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final LayerNode other = (LayerNode) obj;
            if (!Objects.equals(this.layer, other.layer)) {
                return false;
            }
            return true;
        }

    }

    class BlobNode implements CaffeNode {

        private final String name;

        private BlobNode(String blobName) {
            this.name = blobName;
        }

        @Override
        public String toString() {
            return "BLOB " + name;
        }


    }
}
