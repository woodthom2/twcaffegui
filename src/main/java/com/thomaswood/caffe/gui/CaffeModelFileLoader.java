/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thomaswood.caffe.gui;

import caffe.Caffe;
import com.google.protobuf.TextFormat;
import static java.awt.SystemColor.text;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author thomaswood
 */
public class CaffeModelFileLoader {

    public static Caffe.NetParameter.Builder loadModel(String prototxtFile) throws IOException, FileNotFoundException {
        Caffe.NetParameter net = Caffe.NetParameter.getDefaultInstance();
        Caffe.NetParameter.Builder builder = Caffe.NetParameter.newBuilder(net);
        FileReader fileReader = new FileReader(prototxtFile);
        TextFormat.merge(fileReader, builder);
        System.out.println("File is " + prototxtFile);
        System.out.println("net name is " + builder.getName());
        return builder;
    }

    public static void saveModel(String prototxtFile, Caffe.NetParameter.Builder modelBuilder) throws IOException, FileNotFoundException {

        String modelString = TextFormat.printToString(modelBuilder);

        PrintWriter out = new PrintWriter(prototxtFile);
        out.println(modelString);

        out.close();

    }

}
